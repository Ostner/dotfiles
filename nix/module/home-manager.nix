{pkgs, ...}: {
  home.file.".emacs.d/early-init.el".source = ../config/emacs/early-init.el;
  home.file.".emacs.d/tree-sitter/libtree-sitter-swift.dylib".source = ../config/emacs/tree-sitter/libtree-sitter-swift.dylib;

  home.packages = with pkgs; [
    bottom
    cmake
    curl
    gnupg
    iosevka-comfy.comfy
    iosevka-comfy.comfy-motion-duo
    jq
    kubectl
    nodejs_18
    pass
    ripgrep
    rnix-lsp
    tree
    wget
  ];

  home.stateVersion = "23.11";

  home.sessionVariables = {
    PAGER = "less";
    CLICOLOR = 1;
    EDITOR = "emacs";
    LOGSEQ_DIR = "~/Logseq/Me/";
  };

  programs = {
    alacritty = {
      enable = true;
      settings.font = {
        size = 14;
        normal.family = "Iosevka Comfy";
        normal.style = "Regular";
      };
      settings.window = {
        padding.x = 18;
        padding.y = 18;
        decorations = "Buttonless";
      };
    };

    emacs = {
      enable = true;
      extraConfig = builtins.readFile ../config/emacs/init.el +
                    builtins.readFile ../config/emacs/lisp/utils.el;
      extraPackages = epkgs: (with epkgs; [
        ace-window
        avy
        cape
        cider
        clojure-mode
        consult
        corfu
        dash
        diff-hl
        dired-preview
        dockerfile-mode
        editorconfig
        ef-themes
        exec-path-from-shell
        f
        flycheck
        flycheck-clj-kondo
        flycheck-swiftlint
        gptel
        git-link
        graphql-mode
        helpful
        js2-mode
        json-mode
        ligature
        minions
        multiple-cursors
        nerd-icons
        nerd-icons-completion
        nerd-icons-corfu
        nerd-icons-dired
        nerd-icons-ibuffer
        ob-restclient
        ob-swift
        olivetti
        orderless
        org
        org-modern
        ox-gfm
        pulsar
        rainbow-delimiters
        rainbow-mode
        restclient
        s
        smartparens
        so-long
        swift-mode
        (trivialBuild {
          pname = "swift-ts-mode";
          version = "main-05-17-2024";
          src = pkgs.fetchFromGitHub {
            owner = "rechsteiner";
            repo = "swift-ts-mode";
            rev = "main";
            hash = "sha256-OrJN33iaY/7IE7bRVw2i1xnnUfc3iznxeAFcxlibx5Q=";
          };
        })
        super-save
        terraform-mode
        treesit-auto
        yaml-mode
        vterm
	      fontaine
        magit
        magit-todos
        marginalia
        modus-themes
        nix-mode
        vertico
        (trivialBuild {
          pname = "copilot.el";
          version = "main-05-12-2024";
          src = pkgs.fetchFromGitHub {
            owner = "copilot-emacs";
            repo = "copilot.el";
            rev = "main";
            hash = "sha256-Knp36PtgA73gtYO+W1clQfr570bKCxTFsGW3/iH86A0=";
          };
          buildInputs = [
            epkgs.dash
            epkgs.editorconfig
            epkgs.s
            epkgs.f
          ];
        })
      ]);
      package = pkgs.emacs29;
    };

    git = {
      enable = true;
      userName = "Tobias Ostner";
      userEmail = "tobias.ostner@gmail.com";
      extraConfig = {
        init.defaultBranch = "main";
        pull.rebase = true;
        rebase.updateRefs = true;
      };
    };

    kitty = {
      enable = true;
      font = {
        size = 14;
        name  = "Iosevka Comfy";
      };
      extraConfig = builtins.readFile ../config/kitty/kitty.conf;
    };

    starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        gcloud.disabled = true;
        aws.disabled = true;
        hostname.style = "bold green";
      };
    };

    zsh = {
      enable = true;
      enableCompletion = true;
      initExtra = ''
        eval "$(/opt/homebrew/bin/brew shellenv)"
      '';
      syntaxHighlighting.enable = true;
      shellAliases = {
        ls = "ls --color=auto -F";
      };
    };
  };

  xdg.configFile."amethyst/amethyst.yml".text = builtins.readFile ../config/amethyst/amethyst.yml;
}
