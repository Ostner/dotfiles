{ hostname }:
{
  nix = {
    settings = {
      auto-optimise-store = true;
      builders-use-substitutes = true;
      experimental-features = ["flakes" "nix-command"];
      substituters = ["https://nix-community.cachix.org"];
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
      trusted-users = ["@wheel"];
      warn-dirty = false;
    };
  };

  programs.zsh.enable = true;
  programs.gnupg.agent.enable = true;

  networking.hostName = hostname;

  homebrew = {
    enable = true;
    onActivation.cleanup = "zap";

    casks = [
      "amethyst"
      "anki"
      "arc"
      "karabiner-elements"
      "logseq"
      "nextcloud"
      "protonvpn"
      "raycast"
    ];
  };

    system = {
    activationScripts.postUserActivation.text = ''
    /System/Library/PrivateFrameworks/SystemAdministration.framework/Resources/activateSettings -u
    '';

    defaults = {
      dock= {
        autohide = true;
        autohide-time-modifier = 0.1;
        expose-animation-duration = 0.0;
        mru-spaces = false;
        orientation = "right";
        show-recents = false;
      };

      finder = {
        AppleShowAllExtensions = true;
        AppleShowAllFiles = true;
        CreateDesktop = false;
        FXPreferredViewStyle = "Nlsv";
        QuitMenuItem = true;
        ShowPathbar = true;
        ShowStatusBar = true;
        _FXShowPosixPathInTitle = true;
      };

      trackpad = {
        Clicking = true;
        TrackpadRightClick = true;
      };

      menuExtraClock = {
        IsAnalog = false;
        Show24Hour = true;
        ShowDate = 0;
        ShowDayOfWeek = false;
      };

      NSGlobalDomain = {
        AppleInterfaceStyle = "Dark";
        AppleShowAllExtensions = true;
        AppleShowAllFiles = true;
        InitialKeyRepeat = 14;
        KeyRepeat = 1;
        "com.apple.mouse.tapBehavior" = 1;
      };
    };
  };
}
