(defun to/xcode-info ()
    (interactive)
    (let ((buffer "*Xcode info*"))
      (shell-command "system_profiler SPDeveloperToolsDataType" buffer buffer)
      (with-current-buffer buffer
        (view-mode +1)
        (select-window (get-buffer-window buffer)))))
