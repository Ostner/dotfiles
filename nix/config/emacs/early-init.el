;; early-init.el --- -*- no-byte-compile: t -*-

(setq byte-compile-warnings '(not obsolete))
(setq warning-suppress-log-types '((comp) (bytecomp)))
(setq native-comp-async-report-warnings-errors 'silent)
(setq inhibit-startup-echo-area-message (user-login-name))

(customize-set-variable 'load-prefer-newer noninteractive)

(setq frame-inhibit-implied-resize t
      frame-resize-pixelwise t
      display-tinme-default-load-average nil)

(setq inhibit-startup-message t)
(setq initial-scratch-message nil)

(push '(tool-bar-lines . 0) default-frame-alist)
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)
(push '(mouse-color . "white") default-frame-alist)

(scroll-bar-mode -1)
(menu-bar-mode -1)

(add-to-list 'default-frame-alist '(undecorated . t))

(customize-set-variable 'initial-major-mode 'emacs-lisp-mode)
