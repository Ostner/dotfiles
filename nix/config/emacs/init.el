(add-hook 'emacs-startup-hook
          (lambda ()
            (message "tobmacs loaded in %s."
                     (emacs-init-time))))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file 'noerror)

;; (setq package-archives
;;       '(("elpa" . "https://elpa.gnu.org/packages/")
;;         ("elpa-devel" . "https://elpa.gnu.org/devel/")
;;         ("nongnu" . "https://elpa.nongnu.org/nongnu/")
;;         ("melpa" . "https://melpa.org/packages/")))

;; (setq package-archive-priorities
;;       '(("elpa" . 2)
;;         ("nongnu" . 1)))

;; (eval-when-compile
;;   (require 'use-package)
;;   (setq use-package-enable-imenu-support t)
  ;; (setq use-package-compute-statistics t)
;; (setq use-package-verbose t))

(require 'bind-key)

;; (add-to-list 'load-path (expand-file-name "lisp/" user-emacs-directory))
;; (require 'utils)

(use-package ace-window
  :bind
  ("M-o" . ace-window)
  :config
  (setq aw-keys '(?a ?b ?c ?d ?e ?f ?g ?h ?i ?j)))

(use-package autorevert
  :config
  (setq auto-revert-check-vc-info t
        auto-revert-interval 1)
  (global-auto-revert-mode))

(use-package avy
  :bind
  ("C-." . avy-goto-char-timer))

(use-package cape
  :init
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-history))

(use-package cider
  :defer t)

(use-package clojure-mode
  :defer t
  :config
  (use-package flycheck-clj-kondo)
  (require 'flycheck-clj-kondo))

(use-package consult
  :bind
  (("C-x b" . consult-buffer)
   ("C-x 4 b" . consult-buffer-other-window)
   ("C-x r b" . consult-bookmark)
   ("C-x p b" . consult-project-buffer)
   ("M-y" . consult-yank-pop)
   ("C-c k" . consult-kmacro)
   ("M-g g" . consult-goto-line)
   ("M-g M-g" . consult-outline)
   ("M-i" . consult-imenu)
   ("M-s l" . consult-line)
   ("M-s L" . consult-line-multi))
  :hook
  (completion-list-mode . consult-preview-at-point-mode))

(use-package copilot
  :bind ("M-C-<return>" . copilot-accept-completion)
  :commands (copilot-login global-copilot-mode)
  :config
  (use-package dash)
  (use-package editorconfig)
  (use-package f)
  (use-package s)

  (setq copilot-indent-offset-warning-disable t)
  (defun to/copilot-disable-predicate ()
    (member major-mode '(shell-mode
                         eshell-mode
                         term-mode
                         vterm-mode
                         compilation-mode
                         debugger-mode
                         dired-mode-hook
                         compilation-mode-hook
                         minibuffer-mode-hook)))
  (add-to-list 'copilot-disable-predicates #'to/copilot-disable-predicate))

(use-package corfu
  :custom
  (corfu-cycle t)
  (corfu-preselect-first nil)
  (corfu-quit-no-match 'separator)
  :init
  (global-corfu-mode)
  :config
  (add-hook 'eshell-mode-hook
          (lambda ()
            (setq-local corfu-auto nil)
            (corfu-mode))))

(use-package diff-hl
  :hook ((magit-pre-refresh . diff-hl-magit-pre-refresh)
         (magit-post-refresh . diff-hl-magit-post-refresh))
  :config
  (global-diff-hl-mode))

(use-package dired
  :init
  (setq dired-dwim-target t
        dired-hide-details-hide-symlink-targets nil
        dired-auto-revert-buffer #'dired-directory-changed-p
        dired-recursive-copies 'always
        dired-recursive-deletes 'top
        dired-kill-when-opening-new-dired-buffer t)

  (use-package dired-preview
    :config
    (setq dired-preview-delay 2)
    (dired-preview-global-mode 1))

  (add-hook 'dired-mode-hook #'dired-hide-details-mode)
  (add-hook 'dired-mode-hook #'hl-line-mode))

(use-package dockerfile-mode
  :defer t)

(use-package ef-themes
  :config
  (setq ef-themes-headings
        '((0 . (variable-pitch light 2.0))
          (1 . (variable-pitch light 1.7))
          (2 . (variable-pitch semibold 1.5))
          (t . (variable-pitch 1.0))))
  (setq ef-themes-mixed-fonts t
        ef-themes-variable-pitch-ui t)
  (mapc #'disable-theme custom-enabled-themes))

(use-package ediff
  :config
  (setq
   ediff-keep-variants nil
   ediff-make-buffers-readonly-at-startup nil
   ediff-show-clashes-only t
   ediff-split-window-function 'split-window-horizontally
   ediff-window-setup-function 'ediff-setup-windows-plain))

(use-package eglot
  :hook
  (swift-mode . eglot-ensure)
  (nix-mode . eglot-ensure)
  :config
  (setq eglot-autoshutdown t
        eglot-send-idle-time 0.1)
  (add-to-list 'eglot-server-programs '(swift-mode . ("xcrun" "sourcekit-lsp")))
  (add-to-list 'eglot-server-programs '(swift-ts-mode . ("xcrun" "sourcekit-lsp")))
  (add-to-list 'eglot-server-programs '(nix-mode . ("rnix-lsp"))))

(use-package emacs
  :ensure nil
  :custom
  (visible-bell t)
  (global-auto-revert-non-file-buffers t)
  (kill-do-not-save-duplicates t)
  (fast-but-imprecise-scrolling t)
  (scroll-conservatively 101)
  (scroll-margin 0)
  (scroll-preserve-screen-position t)
  :config
  (minibuffer-electric-default-mode)
  (repeat-mode)
  ;; (fset 'yes-or-no-p 'y-or-n-p)
  (delete-selection-mode)
  (pixel-scroll-precision-mode)

  (setq
   auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
   backup-directory-alist `((".*" . ,temporary-file-directory))
   coding-system-for-read 'utf-8
   coding-system-for-write 'utf-8
   create-lockfiles nil
   default-process-coding-system '(utf-8-unix . utf-8-unix)
   delete-by-moving-to-trash t
   isearch-allow-motion t
   locale-coding-system 'utf-8
   next-line-add-newlines nil
   sentence-end-double-space nil
   user-full-name "Tobias Ostner"
   user-mail-address "tobias.ostner@gmail.com"
   undo-limit 6710886400 ;; 64mb
   undo-strong-limit 100663296 ;; x 1.5 (96mb)
   undo-outer-limit 1006632960 ;; x 10 (960mb)
   truncate-string-ellipsis "..."
   use-dialog-box nil
   fast-but-imprecise-scrolling t
   read-process-output-max (* 1024 1024)
   ns-use-proxy-icon nil
   frame-title-format nil
   auto-window-vscroll nil
   bookmark-fringe-mark nil
   tab-always-indent 'complete
   echo-keystrokes 0.1
   history-delete-duplicates t)

  (setq isearch-lazy-count t
        lazy-count-prefix-format "(%s/%s) "
        lazy-count-suffix-format nil
        search-whitespace-regexp ".*?")

  (setq-default indent-tabs-mode nil
                bidi-paragraph-direction 'left-to-right
                bidi-inhibit-bpa t)

  (set-charset-priority 'unicode)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (set-selection-coding-system 'utf-8)
  (prefer-coding-system 'utf-8)

  (require 'epg)
  (setq epg-pinentry-mode 'loopback)

  (when (fboundp 'mac-auto-operator-composition-mode)
    (mac-auto-operator-composition-mode))

  (when (memq window-system '(mac ns))
    (setq mac-option-modifier 'none
          mac-command-modifier 'meta
          visible-bell nil
          ring-bell-function 'flash-mode-line)

    (defun flash-mode-line ()
      (invert-face 'mode-line)
      (run-with-timer 0.1 nil #'invert-face 'mode-line))

    (setq frame-resize-pixelwise t)
    (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
    (add-to-list 'default-frame-alist '(ns-appearance . dark))

    (use-package exec-path-from-shell
      :config
      (exec-path-from-shell-initialize)))

  (add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)
  (add-hook 'package-menu-mode-hook #'hl-line-mode))

(use-package flycheck
  :defer 3)

(use-package flycheck-swiftlint
  :after flycheck
  :config
  (flycheck-swiftlint-setup))

(use-package frame
  :init
  (keymap-global-unset "C-z" nil))

(use-package fontaine
  :config
  (setq-default text-scale-remap-header-line t)
  (setq fontaine-presets
        '((regular)
          (presentation
           :default-weight semilight
           :default-height 250)
          (t
           :default-family "Iosevka Comfy"
           :default-weight regular
           :default-height 140
           :fixed-pitch-height 1.0
           :fixed-pitch-serif-height 1.0
           :variable-pitch-family "Iosevka Comfy Motion Duo"
           :variable-pitch-height 1.0
           :bold-weight bold
           :italic-slant italic
           :line-spacing 0.2)))
  (add-hook 'kill-emacs-hook #'fontaine-store-latest-preset)
  (dolist (hook '(modus-themes-after-load-theme-hook ef-themes-post-load-hook))
    (add-hook hook #'fontaine-apply-current-preset))
  (dolist (hook '(text-mode-hook help-mode-hook))
    (add-hook hook (lambda ()
                     (unless (or (derived-mode-p 'mhtml-mode 'nxml-mode 'yaml-mode)
                                 (member (buffer-name) '("*Colors*" "*Faces*" "*Quick Help*")))
                       (variable-pitch-mode)))))
  (fontaine-set-preset (or (fontaine-restore-latest-preset) 'regular)))

(use-package gptel
  :bind ("C-c RET" . gptel-send)
  :config
  (setq gptel-api-key
        (lambda ()
          (nth 0 (process-lines "pass" "show" "openai-api"))))
  (setq gptel-default-mode 'org-mode))

(use-package git-link
  :defer t)

(use-package graphql-mode
  :defer t)

(use-package helpful
  :bind (("C-h f" . helpful-callable)
         ("C-h v" . helpful-variable)))

(use-package hl-line
  :hook (prog-mode . hl-line-mode))

(use-package js2-mode
  :defer t
  :custom
  (js2-basic-offset 2)
  (js-chain-indent t)
  (js-indent-level 2))

(use-package json-mode
  :defer t)

(use-package ligature
  :after fontaine
  :config
  (defun to/set-ligatures-for-font-iosevka ()
    (when (string= "Iosevka Comfy" (face-attribute 'default :family))
      (ligature-set-ligatures
       'prog-mode '("<---" "<--"  "<<-" "<-" "->" "-->" "--->" "<->" "<-->" "<--->" "<---->" "<!--"
                    "<==" "<===" "<=" "=>" "=>>" "==>" "===>" ">=" "<=>" "<==>" "<===>" "<====>" "<!---"
                    "<~~" "<~" "~>" "~~>" "::" ":::" "==" "!=" "===" "!=="
                    ":=" ":-" ":+" "<*" "<*>" "*>" "<|" "<|>" "|>" "+:" "-:" "=:" "<******>" "++" "+++"))))

  (to/set-ligatures-for-font-iosevka)
  (global-ligature-mode t))

(use-package magit
  :defer t)

(use-package magit-todos
  :after magit
  :config
  (magit-todos-mode 1))

(use-package marginalia
  :config
  (marginalia-mode))

(use-package modus-themes
  :config
  (setq modus-themes-org-blocks 'tinted-background
        modus-themes-bold-constructs t
        modus-themes-italic-constructs t
        modus-themes-completions '((t . (extrabold)))
        modus-themes-to-toggle '(modus-operandi modus-vivendi)
        modus-themes-mixed-fonts t
        modus-themes-variable-pitch-ui nil
        modus-themes-headings
        '((0 . (variable-pitch light 2.0))
          (1 . (variable-pitch light 1.7))
          (2 . (variable-pitch semibold 1.5))
          (t . (variable-pitch 1.0)))
        modus-themes-common-palette-overrides '((fringe unspecified)
                                                (border-mode-line-active unspecified)
                                                (border-mode-line-inactive unspecified)))
  (load-theme 'modus-vivendi-tinted :no-confirm))

(use-package minions
    :config
    (minions-mode))

(use-package multiple-cursors
  :bind
  (("C-c m n" . mc/mark-next-like-this)
   ("C-c m l" . mc/edit-lines)
   ("C-c m p" . mc/mark-previous-like-this)
   ("C-c m a" . mc/mark-all-like-this)
   (:repeat-map mc/multi-cursors-repeat-map
                ("n" . mc/mark-next-like-this)
                ("p" . mc/mark-previous-like-this))))

(use-package nerd-icons
  :defer 2
  :if window-system
  :config
  (use-package nerd-icons-completion
    :config
    (nerd-icons-completion-mode))
  (use-package nerd-icons-corfu
    :config
    (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))
  (use-package nerd-icons-dired
    :defer t
    :hook dired-mode)
  (use-package nerd-icons-ibuffer
    :defer t
    :hook ibuffer-mode))

(use-package nix-mode
  :defer t)

(use-package ob
  :after org
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp .t)
     (shell . t))))

(use-package ob-restclient
  :after org
  :defer t
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   (append org-babel-load-languages
           '((restclient . t)))))

(use-package ob-swift
  :after org
  :defer 3
  :config
    (org-babel-do-load-languages
     'org-babel-load-languages
     (append org-babel-load-languages
             '((swift . t)))))

(use-package olivetti
  :hook (org-mode markdown-mode)
  :custom
  (olivetti-body-width 90))

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles partial-completion)))))

(use-package org
  :bind
  (("C-c c" . 'org-capture)
   ("C-c a" . 'org-agenda))
  :hook
  (org-mode . visual-line-mode)
  :config
  (require 'org-tempo)
  (setq org-auto-align-tags nil
        org-tags-column 0
        org-special-ctrl-a/e t
        org-pretty-entities t
        org-hide-emphasis-markers t
        org-src-preserve-indentation t
        org-startup-with-inline-images t
        org-agenda-files '("~/Logseq/Me/journals/" "~/Logseq/Me/pages")
        org-capture-templates
        '(("i" "Inbox" entry (file "~/Logseq/Me/pages/Inbox.org")
           "* %?\n"))
        org-confirm-babel-evaluate nil
        org-startup-indented t
        calendar-week-start-day 1))

(use-package org-modern
  :after org
  :config
  (global-org-modern-mode)
  (setq org-modern-table nil))

(use-package ox-gfm
  :defer t
  :after org)

(use-package pulsar
  :config
  (pulsar-global-mode 1)
  (add-hook 'next-error-hook #'pulsar-pulse-line))

(use-package rainbow-delimiters
  :hook (emacs-lisp-mode clojure-mode))

(use-package rainbow-mode
  :hook ((text-mode prog-mode) . rainbow-mode))

(use-package recentf-mode
  :hook (after-init . recentf-mode))

(use-package restclient
  :defer t)

(use-package savehist
  :init
  (savehist-mode))

(use-package smartparens
  :hook ((prog-mode . smartparens-mode))
  :bind
  (:map smartparens-mode-map
        ("C-M-f" . sp-forward-sexp)
        ("C-M-b" . sp-backward-sexp)
        ("C-M-e" . sp-up-sexp)
        ("C-M-u" . sp-backward-up-sexp)
        ("C-M-d" . sp-down-sexp)
        ("C-M-a" . sp-backward-down-sexp)
        ("C-M-t" . sp-transpose-sexp)
        ("C-M-k" . sp-kill-sexp)
        ("C-M-w" . sp-copy-sexp)
        ("C-M-]" . sp-forward-slurp-sexp)
        ("C-M-[" . sp-backward-slurp-sexp)
        ("C-M-}" . sp-forward-barf-sexp)
        ("C-M-{" . sp-backward-barf-sexp)
        ("M-<backspace>" . sp-unwrap-sexp))
  :config
  (require 'smartparens-config)

  (defun to/indent-between-pair (&rest _ignored)
    (message "to/indent-between-pair")
    (newline)
    (indent-according-to-mode)
    (forward-line -1)
    (indent-according-to-mode))

  (sp-local-pair 'prog-mode "{" nil :post-handlers '((to/indent-between-pair "RET")))
  (sp-local-pair 'prog-mode "[" nil :post-handlers '((to/indent-between-pair "RET")))
  (sp-local-pair 'prog-mode "(" nil :post-handlers '((to/indent-between-pair "RET")))

  (sp-local-pair 'swift-mode "\\(" ")" :when '(sp-in-string-p)))

(use-package so-long
  :config
  (global-so-long-mode))

(use-package super-save
  :config
  (super-save-mode))

(use-package swift-mode
  :hook (swift-mode . subword-mode)
  :config
  (set-fill-column 100)
  (setq
   swift-mode:multiline-statement-offset 2
   swift-mode:parenthesized-expression-offset 2
   swift-mode:basic-offset 2
   swift-mode:repl-executable "xcrun swift repl"))

(use-package swift-ts-mode
  :config
  (setq swift-ts-mode-indent-offset 2)
  (add-hook 'swift-ts-mode-hook #'subword-mode))

(use-package tab-bar
  :config
  (tab-bar-mode)
  (setq tab-bar-show t
        tab-bar-close-button-show nil
        tab-bar-new-tab-choice "*scratch*"
        tab-bar-new-button-show nil)
  (set-face-attribute
   'tab-bar nil
   :height 1.0))

(use-package terraform-mode)

(use-package treesit-auto
  :custom
  (treesit-auto-install 'prompt)
  :config
  (add-to-list 'treesit-auto-recipe-list
               (make-treesit-auto-recipe
                :lang 'swift
                :ts-mode 'swift-ts-mode
                :remap '(swift-mode)
                :ext "\\.swift\\'"))
  (setq treesit-auto-langs '(swift))
  (global-treesit-auto-mode))

(use-package yaml-mode)

(use-package vertico
  :config
  (vertico-mode)
  (setq vertico-cycle t))

(use-package vertico-directory
  :after vertico
  :ensure nil
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy)
  :bind (:map vertico-map
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word)))

(use-package vterm
  :defer 2
  :if module-file-suffix)

(use-package whitespace
  :hook (before-save . whitespace-cleanup)
  :config
  (setq whitespace-line-column nil
        whitespace-style '(face empty tabs lines-tail trailing)))

(provide 'init)
;;; init.el ends here
